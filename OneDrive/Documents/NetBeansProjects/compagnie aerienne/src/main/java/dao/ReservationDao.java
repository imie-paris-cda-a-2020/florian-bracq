/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import java.util.List;
import principal.Reservation;
/**
 *
 * @author Flori
 */
public interface ReservationDao {
    Reservation creer();
    Reservation recuperer(int id);
    Reservation modifier(Reservation reservation);

    boolean delete(Reservation reservation);
    List<Reservation>getAllReservation();
}
