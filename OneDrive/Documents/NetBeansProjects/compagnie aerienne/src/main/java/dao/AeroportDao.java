/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import principal.Aeroport;


/**
 *
 * @author Flori
 */
public interface AeroportDao {
    Aeroport creer();
    Aeroport recuperer(int id);
    Aeroport modifier(Aeroport aeroport);
    
    boolean delete(Aeroport aeroport);
    List<Aeroport>getAllAeroport();
}
