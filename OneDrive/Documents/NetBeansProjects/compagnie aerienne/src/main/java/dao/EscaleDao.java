/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import java.util.List;
import principal.Escale;
/**
 *
 * @author Flori
 */
public interface EscaleDao {
    Escale creer();
    Escale recuperer(int id);
    Escale modifier(Escale escale);

    boolean delete(Escale escale);
    List<Escale>getAllEscale(); 
}
