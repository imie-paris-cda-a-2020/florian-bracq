/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author Flori
 */

public class FabriquePersistance {
    
    public static AeroportDao createAeroportDao()
    {
            return new MySQLAeroportDao();
    }
       
    public static ClientDao createClientDao()
    {
            return new MySQLClientDao();
    }
    
    public static CompagnieDao createCompagnieDao()
    {
        return new MySQLCompagnieDao();
    }
    
    public static EscaleDao createEscaleDao()
    {
        return new MySQLEscaleDao();
    }
    
    public static PassagerDao createPassagerDao()
    {
            return new MySQLPassagerDao();
    }
    
    public static PaysDao createPaysDao()
    {
            return new MySQLPaysDao();
    }
    
    public static PersonneDao createPersonneDao()
    {
            return new MySQLPersonneDao();
    }
    
    public static ReservationDao createReservationDao()
    {
            return new MySQLReservationDao();
    }

    public static VolDao createVolDao()
    {
            return new MySQLVolDao();
    }

}
