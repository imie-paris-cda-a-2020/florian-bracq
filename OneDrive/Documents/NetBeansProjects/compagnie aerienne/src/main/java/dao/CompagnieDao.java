/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import principal.Compagnie;

/**
 *
 * @author Flori
 */
public interface CompagnieDao {
    
    Compagnie creer();
    Compagnie recuperer(int id);
    Compagnie modifier(Compagnie compagnie);

    boolean delete(Compagnie compagnie);
    List<Compagnie>getAllCompagnie();
}
