/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import principal.Client;

/**
 *
 * @author Flori
 */
public interface ClientDao {
    
    Client creer();
    Client recuperer(int id);
    Client modifier(Client client);

    boolean delete(Client client);
    List<Client>getAllClient();
}
