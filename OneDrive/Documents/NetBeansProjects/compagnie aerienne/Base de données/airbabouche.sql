-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 10 jan. 2021 à 14:37
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `airbabouche`
--

-- --------------------------------------------------------

--
-- Structure de la table `aeroport`
--

DROP TABLE IF EXISTS `aeroport`;
CREATE TABLE IF NOT EXISTS `aeroport` (
  `idaeroport` varchar(5) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `ville` varchar(70) NOT NULL,
  `pays` int(11) NOT NULL,
  PRIMARY KEY (`idaeroport`),
  KEY `FK_aeroport_pays` (`pays`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `aeroport`
--

INSERT INTO `aeroport` (`idaeroport`, `nom`, `ville`, `pays`) VALUES
('1', 'Air Caraibes', 'PuntaCana', 1),
('2', 'Air france', 'Paris', 1);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `email` varchar(200) NOT NULL,
  `password` varchar(80) NOT NULL,
  `personne_idpersonne` int(11) NOT NULL,
  PRIMARY KEY (`personne_idpersonne`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`email`, `password`, `personne_idpersonne`) VALUES
('totoo@to.to', '$2b$10$rVxPpfe/wTgYNdnNhkPy6uiHN/EG4fa8qDR3M0IBZ224Hl29JBxQe', 5),
('florian@gmal.gn', '$2b$10$gNvh879rC44Zd3vXHNQTAekE/FDG3GNlppxzfRy3FGVoGBhiTzWA.', 7);

-- --------------------------------------------------------

--
-- Structure de la table `compagnie`
--

DROP TABLE IF EXISTS `compagnie`;
CREATE TABLE IF NOT EXISTS `compagnie` (
  `idcompagnie` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY (`idcompagnie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `compagnie`
--

INSERT INTO `compagnie` (`idcompagnie`, `nom`) VALUES
(1, 'air Caraïbes'),
(2, 'air france');

-- --------------------------------------------------------

--
-- Structure de la table `escale`
--

DROP TABLE IF EXISTS `escale`;
CREATE TABLE IF NOT EXISTS `escale` (
  `vol_idvol` int(11) NOT NULL,
  `aeroport_idaeroport` varchar(5) NOT NULL,
  `date_depart` datetime NOT NULL,
  `date_arrive` datetime NOT NULL,
  PRIMARY KEY (`vol_idvol`,`aeroport_idaeroport`),
  KEY `fk_escale_aeroport1_idx` (`aeroport_idaeroport`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `escale`
--

INSERT INTO `escale` (`vol_idvol`, `aeroport_idaeroport`, `date_depart`, `date_arrive`) VALUES
(1, '1', '2021-02-21 13:33:45', '2021-02-21 16:08:12'),
(1, '2', '2021-01-24 12:23:45', '2021-01-25 09:10:50');

-- --------------------------------------------------------

--
-- Structure de la table `passager`
--

DROP TABLE IF EXISTS `passager`;
CREATE TABLE IF NOT EXISTS `passager` (
  `idpassager` int(11) NOT NULL AUTO_INCREMENT,
  `idpersonne` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpassager`),
  KEY `FK_passager_personne` (`idpersonne`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `passager`
--

INSERT INTO `passager` (`idpassager`, `idpersonne`) VALUES
(1, 7);

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

DROP TABLE IF EXISTS `pays`;
CREATE TABLE IF NOT EXISTS `pays` (
  `idPays` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY (`idPays`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `pays`
--

INSERT INTO `pays` (`idPays`, `nom`) VALUES
(1, 'france'),
(4, 'Italie'),
(5, 'Espagne'),
(6, 'Angleterre');

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

DROP TABLE IF EXISTS `personne`;
CREATE TABLE IF NOT EXISTS `personne` (
  `idpersonne` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(70) NOT NULL,
  `prenom` varchar(70) NOT NULL,
  `dateNaiss` date NOT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `pays_idPays` int(11) NOT NULL,
  PRIMARY KEY (`idpersonne`),
  KEY `fk_personne_pays1_idx` (`pays_idPays`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `personne`
--

INSERT INTO `personne` (`idpersonne`, `nom`, `prenom`, `dateNaiss`, `adresse`, `ville`, `zipcode`, `pays_idPays`) VALUES
(5, 'BOB', 'Olivier', '1994-02-02', '12 avenue du général leclerc', 'Perpignan', '66200', 1),
(7, 'FLORIAN', 'florian', '1992-10-06', '106 bld', 'paris', '12458', 1),
(10, 'BRACQ', 'Florian', '1992-01-01', '165 rue des mureaux', 'Paris', '75014', 1),
(11, 'LEBRUN', 'Antoine', '1999-11-12', '106 Boulevard de l\'hopital', 'Orleans', '10025', 1);

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `idvol` int(11) NOT NULL,
  `client_idpersonne` int(11) NOT NULL,
  `confirmation` enum('Confirm','Pending','Cancel','Refuse') NOT NULL,
  `personne_idpersonne` int(11) NOT NULL,
  PRIMARY KEY (`idvol`,`client_idpersonne`),
  KEY `FK_reservation_vol` (`idvol`),
  KEY `fk_reservation_client1_idx` (`client_idpersonne`),
  KEY `fk_reservation_personne1_idx` (`personne_idpersonne`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `reservation`
--

INSERT INTO `reservation` (`idvol`, `client_idpersonne`, `confirmation`, `personne_idpersonne`) VALUES
(1, 5, 'Cancel', 5);

-- --------------------------------------------------------

--
-- Structure de la table `vol`
--

DROP TABLE IF EXISTS `vol`;
CREATE TABLE IF NOT EXISTS `vol` (
  `idvol` int(11) NOT NULL AUTO_INCREMENT,
  `place` smallint(6) NOT NULL DEFAULT 300,
  `intitule` varchar(50) DEFAULT NULL,
  `aeroport_depart` varchar(5) NOT NULL,
  `aeroport_arrive` varchar(5) NOT NULL,
  `date_depart` datetime NOT NULL,
  `date_arrive` datetime NOT NULL,
  `idcompagnie` int(11) NOT NULL,
  PRIMARY KEY (`idvol`),
  KEY `FK_vol_aeroport` (`aeroport_depart`),
  KEY `FK_vol_aeroport_2` (`aeroport_arrive`),
  KEY `FK_vol_compagnie` (`idcompagnie`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `vol`
--

INSERT INTO `vol` (`idvol`, `place`, `intitule`, `aeroport_depart`, `aeroport_arrive`, `date_depart`, `date_arrive`, `idcompagnie`) VALUES
(1, 600, 'n°00012', '1', '1', '2020-10-06 12:25:20', '2020-10-07 12:25:20', 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `aeroport`
--
ALTER TABLE `aeroport`
  ADD CONSTRAINT `FK_aeroport_pays` FOREIGN KEY (`pays`) REFERENCES `pays` (`idPays`);

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `fk_client_personne1` FOREIGN KEY (`personne_idpersonne`) REFERENCES `personne` (`idpersonne`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `escale`
--
ALTER TABLE `escale`
  ADD CONSTRAINT `fk_escale_aeroport1` FOREIGN KEY (`aeroport_idaeroport`) REFERENCES `aeroport` (`idaeroport`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_escale_vol1` FOREIGN KEY (`vol_idvol`) REFERENCES `vol` (`idvol`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `passager`
--
ALTER TABLE `passager`
  ADD CONSTRAINT `FK_passager_personne` FOREIGN KEY (`idpersonne`) REFERENCES `personne` (`idpersonne`);

--
-- Contraintes pour la table `personne`
--
ALTER TABLE `personne`
  ADD CONSTRAINT `fk_personne_pays1` FOREIGN KEY (`pays_idPays`) REFERENCES `pays` (`idPays`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `FK_reservation_vol` FOREIGN KEY (`idvol`) REFERENCES `vol` (`idvol`),
  ADD CONSTRAINT `fk_reservation_client1` FOREIGN KEY (`client_idpersonne`) REFERENCES `client` (`personne_idpersonne`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_reservation_personne1` FOREIGN KEY (`personne_idpersonne`) REFERENCES `personne` (`idpersonne`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `vol`
--
ALTER TABLE `vol`
  ADD CONSTRAINT `FK_vol_aeroport` FOREIGN KEY (`aeroport_depart`) REFERENCES `aeroport` (`idaeroport`),
  ADD CONSTRAINT `FK_vol_aeroport_2` FOREIGN KEY (`aeroport_arrive`) REFERENCES `aeroport` (`idaeroport`),
  ADD CONSTRAINT `FK_vol_compagnie` FOREIGN KEY (`idcompagnie`) REFERENCES `compagnie` (`idcompagnie`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
